import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ItemScreen3 extends StatefulWidget {
  ItemScreen3({Key? key}) : super(key: key);

  @override
  _ItemScreen3State createState() => _ItemScreen3State();
}

class _ItemScreen3State extends State<ItemScreen3> {
  @override
  Widget build(BuildContext context) {
    return new Container(
        decoration: new BoxDecoration(color: Colors.indigo[400]),
        child: new Center(
          child: Text('Item 3!'),
        ));
  }
}
