import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ItemScreen1 extends StatefulWidget {
  ItemScreen1({Key? key}) : super(key: key);

  @override
  _ItemScreen1State createState() => _ItemScreen1State();
}

class _ItemScreen1State extends State<ItemScreen1> {
  @override
  Widget build(BuildContext context) {
    return new Container(
        decoration: new BoxDecoration(color: Colors.cyan[700]),
        child: new Center(
          child: Text(
            'Item 1!',
          ),
        ));
  }
}
