import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ItemScreen2 extends StatefulWidget {
  ItemScreen2({Key? key}) : super(key: key);

  @override
  _ItemScreen2State createState() => _ItemScreen2State();
}

class _ItemScreen2State extends State<ItemScreen2> {
  @override
  Widget build(BuildContext context) {
    return new Container(
      decoration: new BoxDecoration(color: Colors.blue[700]),
      child: new Center(
        child: Text('Item 2!'),
      ),
    );
  }
}
