import 'package:flutter/material.dart';

import 'itemscreen1.dart';
import 'itemscreen2.dart';
import 'itemscreen3.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);
  static const appTitle = 'Drawer Demo';
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: appTitle,
      home: MyHomePage(title: appTitle),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key? key, required this.title}) : super(key: key);
  final String title;
  @override
  _MyHomePageState createState() => _MyHomePageState(title: title);
}

class _MyHomePageState extends State<MyHomePage> {
  _MyHomePageState({required this.title});
  final String title;
  Widget body = const Center(
    child: Text('My Page!'),
  );
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(title),
      ),
      body: body,
      drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: [
            DrawerHeader(
              child: Text('Dreawer Header'),
            ),
            ListTile(
              title: const Text('item 1'),
              onTap: () {
                setState(() {
                  body = ItemScreen1();
                });
                Navigator.pop(context); // Hide NapBar after on click to Item 1
              },
            ),
            ListTile(
              title: const Text('item 2'),
              onTap: () {
                setState(() {
                  body = ItemScreen2();
                });
                Navigator.pop(context); // Hide NapBar after on click to Item 2
              },
            ),
            ListTile(
              title: const Text('item 3'),
              onTap: () {
                setState(() {
                  body = ItemScreen3();
                });
                Navigator.pop(context); // Hide NapBar after on click to Item 3
              },
            ),
          ],
        ),
      ),
    );
  }
}
